﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class Book{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Author { get; set; }
}

[Route("api/[controller]")]
public class BookController : Controller
{
    private List<Book> Books;
    public BookController()
    {
        Books = new List<Book>()
        {
            new Book() {Id=1, Name = "Witcher", Author="Andrzej Sapkowski" },
            new Book() {Id=2, Name = "The Lord of The Rings", Author="John R. R. Tolkien" },
            new Book() {Id=3, Name = "Le Petit Prince", Author="Antoine de Saint-Exupery" },
            new Book() {Id=4, Name = "Rich Daddy", Author="I_Don't_Remember"}
        };
    }

    [HttpGet]
    public IActionResult Get()
    {
        return new ObjectResult(Books);
    }

    [HttpGet("{id}", Name = "GetBook")]
    public IActionResult Get(int id)
    {
        Book book = (from b in Books where b.Id == id select b).FirstOrDefault();
        if (book == null)
        {
            return NotFound();
        }
        else
        {
            return new ObjectResult(book);
        }
    }

    [HttpPost]
    public IActionResult Post([FromBody]Book book)
    {
        if (book == null)
        {
            return BadRequest();
        }
        int nextId = (from b in Books select b.Id).Max() + 1;
        book.Id = nextId;
        Books.Add(book);
        return CreatedAtRoute("GetBook", new { id = nextId }, book);
    }

    /*[HttpDelete]
     public IActionResult Delete(int ind)
     {

     }*/


}